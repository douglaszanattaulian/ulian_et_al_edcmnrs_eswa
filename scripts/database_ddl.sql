CREATE TABLE private.articles (
	id text NOT NULL,
	popularity numeric NULL,
	CONSTRAINT articles_pkey PRIMARY KEY (id)
)
WITH (
	OIDS=FALSE
)
TABLESPACE pg_default;

ALTER TABLE private.articles
    OWNER to "";

-- Index: articles_id_idx

-- DROP INDEX private.articles_id_idx;

CREATE INDEX articles_id_idx
    ON private.articles USING btree
    (id COLLATE pg_catalog."default")
    TABLESPACE pg_default;

-- Table: private.articles_tags

DROP TABLE private.articles_tags;

CREATE TABLE private.articles_tags
(
    article_id text COLLATE pg_catalog."default" NOT NULL,
    tag_id numeric NOT NULL
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE private.articles_tags
    OWNER to "";

-- Index: articles_tags_article_id_tag_id_idx

-- DROP INDEX private.articles_tags_article_id_tag_id_idx;

CREATE INDEX articles_tags_article_id_tag_id_idx
    ON private.articles_tags USING btree
    (article_id COLLATE pg_catalog."default", tag_id)
    TABLESPACE pg_default;
    
    
    
-- Table: private.tags

-- DROP TABLE private.tags;

CREATE TABLE private.tags
(
    id numeric,
    name text COLLATE pg_catalog."default" NOT NULL,
    no_plural text COLLATE pg_catalog."default",
    CONSTRAINT tags_pkey PRIMARY KEY (name)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE private.tags
    OWNER to "";

-- Index: tags_id_idx

-- DROP INDEX private.tags_id_idx;

CREATE INDEX tags_id_idx
    ON private.tags USING btree
    (id)
    TABLESPACE pg_default;


-- Table: private.events

DROP TABLE private.events;

CREATE TABLE private.events
(
    eventosid numeric NOT NULL,
    data_evento timestamp NOT NULL,
    email text COLLATE pg_catalog."default" NOT NULL,
    article_id text COLLATE pg_catalog."default",
    CONSTRAINT events_pkey PRIMARY KEY (eventosid)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE private.events
    OWNER to "";

-- Index: events_email_idx

-- DROP INDEX private.events_email_idx;

CREATE INDEX events_email_idx
    ON private.events USING btree
    (email COLLATE pg_catalog."default")
    TABLESPACE pg_default;

-- Index: events_eventosid_idx

-- DROP INDEX private.events_eventosid_idx;

CREATE INDEX events_eventosid_idx
    ON private.events USING btree
    (eventosid)
    TABLESPACE pg_default;